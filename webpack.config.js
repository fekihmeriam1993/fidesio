var path = require("path");
var webpack = require("webpack");
var autoprefixer = require("autoprefixer");
var precss = require("precss");

module.exports = {
  context: path.resolve(__dirname, "src"),
  entry: "./app.js",
  mode: "development",
  output: {
    path: path.resolve(__dirname, "public"),
    filename: "main.js",
  },

  devServer: {
    static: {
      directory: path.join(__dirname, "public"),
    },
    compress: true,
    port: 9000,
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new webpack.NoErrorsPlugin(), not needed any more
    new webpack.LoaderOptionsPlugin({
      options: {
        context: __dirname,
        postcss: [autoprefixer],
      },
    }),
  ],
  module: {
    rules: [
      {
        test: /\.s?css$/,
        use: ["style-loader", "css-loader", "sass-loader", "postcss-loader"],
      },
    ],
  },
};
