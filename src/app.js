import alertMe from "./helper";
import "./app.scss";

document.querySelectorAll(".accordion-item-title").forEach((elem) =>
  elem.addEventListener("click", (e) => {
    const activeElement = document.querySelector(".accordion-item.active");
    if (activeElement) {
      activeElement.classList.remove("active");
    }
    elem.closest(".accordion-item").classList.toggle("active");
  })
);

document.querySelectorAll(".close-arrow").forEach((elem) => {
  elem.addEventListener("click", () => {
    elem.closest(".accordion-item").classList.remove("active");
  });
});
